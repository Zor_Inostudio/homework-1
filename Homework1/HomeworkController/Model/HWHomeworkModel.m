//
//  HWHomeworkModel.m
//  Homework1
//
//  Created by Vladislav Grigoriev on 29/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#import "HWHomeworkModel.h"
#import "HWHomework.h"

#import "HWMatrixViewController.h"
#import "HWConverterViewController.h"
#import "HWAnimationViewController.h"

@implementation HWHomeworkModel

- (instancetype)init {
    self = [super init];
    if (self) {
        _controllerClasses = @[
                               [HWMatrixViewController class],
                               [HWConverterViewController class],
                               [HWAnimationViewController class],
                               ];
    }
    return self;
}

- (void)pointForTime:(NSTimeInterval)time
                size:(CGSize)size
      withCompletion:(void(^)(CGPoint point))completion {
    dispatch_async(dispatch_get_main_queue(), ^{
        HWPoint point = pointForAnimationTime(time, size.width);
        if (completion) {
            completion(CGPointMake(point.x, point.y));
        }
    });
}

- (void)convertString:(NSString *)string
       withCompletion:(void(^)(NSString *convertedString))completion {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        const char * result = stringForNumberString([string cStringUsingEncoding:NSUTF8StringEncoding]);
        
        NSString *resultString = result != NULL ? [NSString stringWithCString:result encoding:NSUTF8StringEncoding] : nil;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completion) {
                completion(resultString);
            }
        });
    });
}

- (void)createMatrixWithRowsCount:(NSInteger)rowsCount
                     columnsCount:(NSInteger)columnsCount
                       completion:(void(^)(NSArray<NSArray<NSNumber *> *> *matrix))completion {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        long **matrix = createMatrix(rowsCount, columnsCount);
        
        if (matrix != NULL) {
            NSMutableArray<NSArray<NSNumber *> *> *mutableArrays = [[NSMutableArray alloc] initWithCapacity:rowsCount];
            for (NSInteger i = 0; i < rowsCount; ++i) {
                NSMutableArray<NSNumber *> *mutableNumbers = [[NSMutableArray alloc] initWithCapacity:columnsCount];
                for (NSInteger j = 0; j < columnsCount; ++j) {
                    [mutableNumbers addObject:@(matrix[i][j])];
                }
                [mutableArrays addObject:[mutableNumbers copy]];
            }
            free(matrix);
            dispatch_async(dispatch_get_main_queue(), ^{
                if (completion) {
                    completion([mutableArrays copy]);
                }
            });
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (completion) {
                    completion(nil);
                }
            });
        }
    });
}

@end
